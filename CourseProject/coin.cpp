/*  bsm1998   */
#include "mlisp.h"
double dd();
double mm();
double LARGEST__COIN();
double one();
double fifty();
double five();
double three();
double two();
double hungrand();
double cc(double amount, double largest__coin);
double count__change(double amount);
double next__coin(double coin);
double GR__AMOUNT();
//________________ 
double dd() {
return 31;
}
double mm() {
return 7;
}
double LARGEST__COIN() {
return 50;
}
double one() {
return 1;
}
double fifty() {
return 50;
}
double five() {
return 5;
}
double three() {
return 3;
}
double two() {
return 2;
}
double hungrand() {
return 100;
}
double cc(double amount, double largest__coin) {
return (( ((amount == 0) || (largest__coin == one())) ) ? (one()) :
( true ) ? ((( ((amount < 0) || (largest__coin < 0)) ) ? (0) :
( true ) ? ((cc(amount, next__coin(largest__coin)) + cc((amount - largest__coin), largest__coin))) :
 _infinity)) :
 _infinity);
}
double count__change(double amount) {
return cc(amount, LARGEST__COIN());
}
double next__coin(double coin) {
return (( (coin == fifty()) ) ? (five()) :
( true ) ? ((( (coin == five()) ) ? (three()) :
( true ) ? ((( (coin == three()) ) ? (two()) :
( true ) ? ((( (coin == two()) ) ? (one()) :
( true ) ? (0) :
 _infinity)) :
 _infinity)) :
 _infinity)) :
 _infinity);
}
double GR__AMOUNT() {
return ((hungrand() * mm()) + dd());
}
int main(){
display(" bsm variant 4");
newline();
display(" 1-2-3-5-50");
newline();
display("count-change for 100 \t= ");
display(count__change(hungrand()));
newline();
display("count-change for ");
display(GR__AMOUNT());
display(" \t= ");
display(count__change(GR__AMOUNT()));
newline();
std::cin.get();
return 0;
}

