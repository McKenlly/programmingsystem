/*  bsm1998   */
#include "mlisp.h"
double one();
double two();
double tf();
double seven();
double yyyyy();
double ddd();
double mmm();
double even__bits(double n);
double odd__bits(double n);
double display__bin(double n);
double report__results(double n);
double dd();
double mm();
double yyyy();
//________________ 
double one() {
return 1;
}
double two() {
return 2;
}
double tf() {
return 31;
}
double seven() {
return 7;
}
double yyyyy() {
return 1998;
}
double ddd() {
return 1000000;
}
double mmm() {
return 10000;
}
double even__bits(double n) {
return (( (n == 0) ) ? (one()) :
( true ) ? ((( (remainder(n, two()) == 0) ) ? (even__bits(quotient(n, two()))) :
( true ) ? (odd__bits(quotient(n, two()))) :
 _infinity)) :
 _infinity);
}
double odd__bits(double n) {
return (( (n == 0) ) ? (0) :
( true ) ? ((( (remainder(n, two()) == 0) ) ? (odd__bits(quotient(n, two()))) :
( true ) ? (even__bits(quotient(n, two()))) :
 _infinity)) :
 _infinity);
}
double display__bin(double n) {
display(remainder(n, two()));
return (( (n == 0) ) ? (0) :
( true ) ? (display__bin(quotient(n, two()))) :
 _infinity);
}
double report__results(double n) {
display("Happy birthday to you!\n\t");
display(n);
display(" (decimal)\n\t");
display("\teven?\t");
display((even__bits(n) == one()) ?"yes":"no");
newline();
display("\todd?\t");
display((odd__bits(n) == one()) ?"yes":"no");
newline();
n = display__bin(n);
display("(reversed binary)\n");
return 0;
}
double dd() {
return 31;
}
double mm() {
return 7;
}
double yyyy() {
return 1998;
}
int main(){
display(report__results(((dd() * ddd()) + (mm() * mmm()) + yyyy()))); newline();
std::cin.get();
return 0;
}

