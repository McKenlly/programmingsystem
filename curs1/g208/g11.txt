# $g11
    $id    $idq    $dec   $zero 
  $bool    $str       (       ) 
      +       -       *       / 
      <       =       >      <= 
     >=     and     not      or 
   cond    else      if     let 
 define display newline    set! 

#
      S -> PROG #1
   PROG -> CALCS1 #2 |
           DEFS #3 |
           DEFS CALCS1 #4
 CALCS1 -> CALCS #5
  CALCS -> CALC #6 |
           CALCS CALC #7
   CALC -> E1 #8 |
           BOOL #9 |
           STR #10 |
           DISPSET #11
     E1 -> E #12
      E -> $id #13 |
           CONST #14 |
           ADD #15 |
           SUB #16 |
           DIV #17 |
           MUL #18 |
           IF #19 |
           CPROC #20
  CONST -> $zero #21 |
           $dec #22
    ADD -> HADD E1 ) #23
   HADD -> ( + #24 |
           HADD E1 #25
    SUB -> HSUB E1 ) #26
   HSUB -> ( - #27 |
           HSUB E1 #28
    DIV -> HDIV E1 ) #29
   HDIV -> ( / E #30
    MUL -> HMUL E1 ) #31
   HMUL -> ( * #32 |
           HMUL E1 #33
     IF -> IFTRUE E1 ) #34
 IFTRUE -> HIF E #35
    HIF -> ( if BOOL #36
  CPROC -> HCPROC ) #37
 HCPROC -> ( $id #38 |
           HCPROC E #39
   BOOL -> $bool #40 |
           CPRED #41 |
           REL #42 |
           AND #43 |
           ( not BOOL ) #44
  CPRED -> HCPRED ) #45
 HCPRED -> ( $idq #46 |
           HCPRED E #47
    REL -> HREL E1 ) #48
   HREL -> ( = E #49 |
           ( > E #50
    AND -> HAND BOOL ) #51
   HAND -> ( and #52 |
           HAND BOOL #53
    STR -> $str #54 |
           SIF #55
    SIF -> SIFTRUE STR ) #56
SIFTRUE -> HIF STR #57
    SET -> HSET E1 ) #58
   HSET -> ( set! $id #59
DISPSET -> ( display E1 ) #60 |
           ( display BOOL ) #61 |
           ( display STR ) #62 |
           ( newline ) #63 |
           SET #64
   DEFS -> DEF #65 |
           DEFS DEF #66
    DEF -> PRED #67 |
           VAR #68 |
           PROC #69
   PRED -> HPRED BOOL ) #70
  HPRED -> PDPAR ) #71
  PDPAR -> ( define ( $idq #72 |
           PDPAR $id #73
    VAR -> VARINI ) #74
 VARINI -> HVAR CONST #75
   HVAR -> ( define $id #76
   PROC -> HPROC LETLOC ) #77 |
           HPROC E1 ) #78
  HPROC -> PCPAR ) #79 |
           HPROC DISPSET #80
  PCPAR -> ( define ( $id #81 |
           PCPAR $id #82
 LETLOC -> HLETLOC E1 ) #83
HLETLOC -> LTVAR ) #84 |
           HLETLOC DISPSET #85
  LTVAR -> ( let ( CPROC #86 |
           LTVAR CPROC #87

