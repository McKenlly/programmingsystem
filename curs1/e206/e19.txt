# $e19
    $id    $idq    $dec   $zero 
  $bool    $str       (       ) 
      +       -       *       / 
      <       =       >      <= 
     >=     and     not      or 
   cond    else      if     let 
 define display newline    set! 

#
      S -> PROG #1
   PROG -> CALCS1 #2 |
           DEFS #3 |
           DEFS CALCS1 #4
 CALCS1 -> CALCS #5
  CALCS -> CALC #6 |
           CALCS CALC #7
   CALC -> E1 #8 |
           BOOL #9 |
           STR #10 |
           DISPSET #11
     E1 -> E #12
      E -> $id #13 |
           $zero #14 |
           ADD #15 |
           SUB #16 |
           DIV #17 |
           MUL #18 |
           COND #19 |
           CPROC #20
    ADD -> HADD E1 ) #21
   HADD -> ( + #22 |
           HADD E1 #23
    SUB -> ( - E1 ) #24
    DIV -> HDIV E1 ) #25
   HDIV -> ( / #26 |
           HDIV E1 #27
    MUL -> HMUL E1 ) #28
   HMUL -> ( * #29 |
           HMUL E1 #30
   COND -> HCOND CLAUS ) #31
  HCOND -> ( cond #32 |
           HCOND CLAUS #33
  CLAUS -> HCLAUS E1 ) #34
 HCLAUS -> ( BOOL #35 |
           HCLAUS DISPSET #36
   ELSE -> HELSE E1 ) #37
  HELSE -> ( else #38 |
           HELSE DISPSET #39
  CPROC -> HCPROC ) #40
 HCPROC -> ( $id #41 |
           HCPROC E #42
   BOOL -> $bool #43 |
           CPRED #44 |
           REL #45 |
           OR #46 |
           AND #47 |
           ( not BOOL ) #48
  CPRED -> HCPRED ) #49
 HCPRED -> ( $idq #50 |
           HCPRED E #51
    REL -> HREL E1 ) #52
   HREL -> ( >= E #53 |
           ( = E #54
     OR -> HOR BOOL ) #55
    HOR -> ( or #56 |
           HOR BOOL #57
    AND -> HAND BOOL ) #58
   HAND -> ( and #59 |
           HAND BOOL #60
    STR -> $str #61
    SET -> HSET E1 ) #62
   HSET -> ( set! $id #63
DISPSET -> ( display E1 ) #64 |
           ( display BOOL ) #65 |
           ( display STR ) #66 |
           ( newline ) #67 |
           SET #68
   DEFS -> DEF #69 |
           DEFS DEF #70
    DEF -> PRED #71 |
           PROC #72
   PRED -> HPRED BOOL ) #73
  HPRED -> PDPAR ) #74
  PDPAR -> ( define ( $idq #75 |
           PDPAR $id #76
   PROC -> HPROC $dec ) #77 |
           HPROC E1 ) #78
  HPROC -> PCPAR ) #79 |
           HPROC DISPSET #80
  PCPAR -> ( define ( $id #81 |
           PCPAR $id #82

