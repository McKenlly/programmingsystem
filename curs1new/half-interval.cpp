/*  bsm1998   */
#include "mlisp.h"
double a__value();
double b__value();
double test__value();
double midpoint();
double one();
double three();
double four();
double five();
double two__double();
double temp();
double tolerance();
double tmp1();
double tmp2();
double half__interval__metod(double a, double b);
double __bsm1998__try(double neg__point, double pos__point);
bool close__enough_Q(double x, double y);
double average(double x, double y);
double root(double a, double b);
double fun(double z);
//________________ 
double a__value() {
return 0;
}
double b__value() {
return 0;
}
double test__value() {
return 0;
}
double midpoint() {
return 0;
}
double one() {
return 1;
}
double three() {
return 3;
}
double four() {
return 4;
}
double five() {
return 5;
}
double two__double() {
return 2.0;
}
double temp() {
return 2.0;
}
double tolerance() {
return 0.000001;
}
double tmp1() {
return 109;
}
double tmp2() {
return 108;
}
double half__interval__metod(double a, double b) {
a__value = fun(a);
;
b__value = fun(b);
;
return ((!((!((a__value < 0)) || !((0 < b__value))))) ? (__bsm1998__try(a, b)) :
(true) ? (((!((!((0 < a__value)) || !((b__value < 0))))) ? (__bsm1998__try(b, a)) :
(true) ? ((b + one())) :
 _infinity)) :
 _infinity);
}
double __bsm1998__try(double neg__point, double pos__point) {
midpoint = average(neg__point, pos__point);
;
test__value = 0;
;
display("+");
return ((close__enough_Q(neg__point, pos__point)) ? (midpoint) :
(true) ? test__value = fun(midpoint);
((((0 < test__value)) ? (__bsm1998__try(neg__point, midpoint)) :
(true) ? ((((test__value < 0)) ? (__bsm1998__try(midpoint, pos__point)) :
(true) ? (midpoint) :
 _infinity)) :
 _infinity)) :
 _infinity);
}
bool close__enough_Q(double x, double y)
{
return (abs((x - y)) < tolerance());
}
double average(double x, double y) {
return ( (x + y) / two__double());
}
double root(double a, double b) {
display("interval=\t[");
display(a);
display(" , ");
display(b);
display("]\n");
temp = half__interval__metod(a, b);
;
newline();
display("discrepancy=\t");
display(fun(temp));
newline();
display("root=\t\t");
display(((temp - b - one()) == 0) ?"[bad]":"[good]");
return temp;
}
double fun(double z) {
z = (z - ( tmp2() / tmp1()) - ( one() / e));
;
return (z - log(z) - five());
}
int main(){
display(" bsm variant 8"); newline();
display(root(four(), five())); newline();
std::cin.get();
return 0;
}

