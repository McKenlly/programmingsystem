//               testadec.cpp
#include <iostream>
#include <iomanip>
#include "fsm.h"

int main(int argc, char *argv[]) {

  tFSM Adec;

  /*  Build machine   */
  addstr(Adec, 0, "+-", 2);
  addrange(Adec, 0, '1', '9', 1);
  addrange(Adec, 1, '0', '9', 1);

  addstr(Adec, 0, "0", 3);
  addstr(Adec, 2, "0", 3);
  addrange(Adec, 2, '1', '9', 1);
  addstr(Adec, 3, ".", 5);

  addstr(Adec, 5, "0", 5);
  addstr(Adec, 1, ".", 5);
  addstr(Adec, 1, "eE", 7);
  addrange(Adec, 6, '0', '9', 6);

  addrange(Adec, 5, '1', '9', 6);
  addstr(Adec, 3, "eE", 7);
  addstr(Adec, 6, "eE", 7);
  addstr(Adec, 7, "+-", 8);
  addrange(Adec, 8, '0', '9', 9);
  addrange(Adec, 9, '0', '9', 9);

//......................
  Adec.final(1);
 Adec.final(5);
 Adec.final(6);
 Adec.final(9);
//......................

  std::cout << "*** bsm Adec "
       << "size = " << Adec.size()
       << " ***" << std::endl;
  std::cout << std::endl;

  while(true) {
    char input[81];
    std::cout << ">";
    std::cin.getline(input,81);
    if(!*input) break;
    int res = Adec.apply(input);
    std::cout << std::setw(res ? res+1 : 0) << "^"
          << std::endl;
  }
  return 0;
}
