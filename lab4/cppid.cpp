//               cppid.cpp
#include <iostream>
#include <iomanip>
#include "fsm.h"

const int SIZE_STRING = 81;

int main()
{
  tFSM fsm;

 /* Build machine */
  addstr(fsm, 0, "_", 1);
  addrange(fsm, 0, 'A', 'Z', 1);
  addrange(fsm, 0, 'a', 'z', 1);
  addstr(fsm, 1, "_", 1);
  addrange(fsm, 1, 'A', 'Z', 1);
  addrange(fsm, 1, 'a' ,'z', 1);
  addrange(fsm, 1, '0', '9', 1);
  fsm.final(1);


  std::cout << "*** cppid  " << "size=" << fsm.size() << " ***\n";
  std::cout << std::endl;

  while(true) {
  	char input[SIZE_STRING];
  	std::cout << ">";
  	std::cin.getline(input, SIZE_STRING);
  	if (*input == 0) {
  		break;
  	}
  	int res = fsm.apply(input);
  	std::cout << std::setw(res ? res + 1 : 0) << "^" << std::endl;
  }
  return 0;
}
