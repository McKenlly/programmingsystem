//  half-interval.cpp 2018
/*
Created by Bokoch Sergey. Var 8.
Expression: z=x-108/109-1/9
Function: 3*z-a*logz-5
Answer: 8. 4.5886 [ 4, 5]
*/

//  half-interval.cpp 2018
/*
(define(fun z)
  (set! z (- z (/ 108 109)(/ e)))
  (-(* 3 z)
    (* 4 (log z))
    5)
)
*/
#include "mlisp.h"

double tolerance = 0.000001;

bool set_E(double &a, double b);
double fun(double z);
double average(double x, double y);
double close_enough_Q(double x, double y);
double __bsm__try(double neg_point, double pos_point);
double half_interval_method(int a, int b);
double root(int a, int b);

bool set_E(double &a, double b) {
    a = b;
    return true;
}

double fun(double z) {
    set_E(z, z-108.0/109.0-1.0/ e);
    return 3*z - 4*log(z) - 5;
}

double average(double x, double y) {
    return (x + y) / 2.0;
}

double close_enough_Q(double x, double y) {
    return abs(x - y) < tolerance;
}

double __bsm__try(double neg_point, double pos_point) {
    {//let
        double midpoint(average(neg_point, pos_point));
        double test_value(0);
        display("+");
        return close_enough_Q(neg_point, pos_point)
               ? midpoint
               : true
                 ? test_value = fun(midpoint), (test_value > 0)
                 ? __bsm__try(neg_point, midpoint)
                                    : (test_value < 0)
                                      ? __bsm__try(midpoint, pos_point)
                                      : midpoint
                 : _infinity;
    }
}

double half_interval_method(int a, int b) {
    {//let
        double a_value(fun(a));
        double b_value(fun(b));
        return (a_value < 0 && b_value > 0) ? __bsm__try(a, b)
                                            : (a_value > 0 && b_value < 0)
                                              ? __bsm__try(b, a)
                                              : b + 1;
    }
}


double root(int a, int b) {
    display("interval=\t[");
    display(a);
    display(" , ");
    display(b);
    display("]");
    newline();
    {//let
        double temp = half_interval_method(a, b);
        newline();
        display("discrepancy=\t");
        display(fun(temp));
        newline();
        display("root=\t\t");
        display((temp - b - 1 == 0) ? "[bad]" : "[good]");
        return temp;
    }
}


//BSM_ROOT
int main() {
    display("BSM var 8");
    newline();
    display(root(4, 5));
    newline();
}
