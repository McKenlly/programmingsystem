//                 testlex.cpp 2017
#include <iostream>
#include <fstream>
#include <iomanip>
#include "lexer.h"
// using namespace std;

const int SIZE_BUFFER = 100;

int main(int argc, char *argv[]) {
    tLexer lex;
/*  *   *   *   *   *   *   *   *   *   *   *   *   *   */
    while (true) {
        char lineBuffer[SIZE_BUFFER];
        std::cout << std::endl << "Source> ";

        *lineBuffer = 0;

        std::cin.getline(lineBuffer, SIZE_BUFFER);

        //correct line?
        if (*lineBuffer == 0) {
            break;
        }

        std::string source_name = std::string(lineBuffer) + ".ss";

        //correct file path?
       bool file = false;
        {
            std::ifstream tmp(source_name.c_str());

            if (tmp) {
                file = true;
            }
        }

        if (!file) {
            source_name = "temp.ss";
            std::ofstream tmp(source_name.c_str());
            tmp << lineBuffer << std::endl;
        }

        // Print source file lines.
        std::cout << std::endl<< "Source file name:"
             << source_name << std::endl;
        {
            std::ifstream tmp(source_name.c_str());
            int lineCount = 0;
            while (tmp) {
                *lineBuffer = 0;
                tmp.getline(lineBuffer, 1000);
                std::cout << std::setw(4) << ++lineCount
                          << "|" << lineBuffer << std::endl;
            }
            std::cout << "_________________\n";
        }
        //open file
        if (!lex.Begin(source_name.c_str())) {
            std::cout << "Can't open file " << source_name << std::endl;
            continue;
        }


        std::cout << lex.Authentication() << " Lexer scan:" << std::endl;

/*  *   *   *   *   *   *   *   *   *   *   *   *   *   */

        while (true) {
          std::string token = lex.GetToken();
          std::string lexeme = lex.GetLexeme();
          std::cout << std::setw(2) << lex.GetLineCount() <<
                          "/" << std::setw(2) << lex.GetStartPos() << ":" <<
                            std::setw(5) << token << "  " << lexeme << std::endl;
          if(token == "#") break;
        }
/*  *   *   *   *   *   *   *   *   *   *   *   *   *   */

        lex.End();
    }
    return 0;
}
